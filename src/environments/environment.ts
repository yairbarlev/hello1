// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBrUoXZgJ35JUOIPkcH-Y9XZnbjAS4Hoco",
    authDomain: "hello-a3629.firebaseapp.com",
    databaseURL: "https://hello-a3629.firebaseio.com",
    projectId: "hello-a3629",
    storageBucket: "hello-a3629.appspot.com",
    messagingSenderId: "503534803461",
    appId: "1:503534803461:web:1ab59e8fe673fab4831a6c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
