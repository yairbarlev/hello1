import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { User } from './interfaces/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  errorCode;
  errorMessage:string
  user:Observable<User | null>;
  login(email:string, password:string){
    this.afAuth
    .signInWithEmailAndPassword(email,password)  /// promise?
    .then(res => {
      console.log(res);
      this.router.navigate(['/books']);

    } ).catch(error =>

      {
        // Handle Errors here.
        this.errorCode = error.code;
        this.errorMessage = error.message;

        console.log(error);
      })



  }
  register(email:string, password:string){
    this.afAuth
    .createUserWithEmailAndPassword(email,password)  /// promise?- יודע לקלוט אירוע אחד לעומת אובסבול שקולט יותר הסיבה לצרכים שונים
    .then(res => {
      console.log(res);
      this.router.navigate(['/books']);

    } )
    .catch(error =>

    {
      // Handle Errors here.
      this.errorCode = error.code;
      this.errorMessage = error.message;

      console.log(error);
    })


  }
logout(){
  this.afAuth.signOut();
  this.router.navigate([`/books`]);
}
getuser():Observable<User | null>{
  return this.user;
}
  constructor(private afAuth:AngularFireAuth,private router:Router) {
    this.user= this.afAuth.authState;
  }
}
