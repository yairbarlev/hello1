import { Observable } from 'rxjs';
import { BlogService } from './../blog.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  blogs$;/** $ - for observable*/
  panelOpenState = false;
  hasError:boolean = false;
  errorMessage:string;
  constructor(private route:ActivatedRoute, private blogService:BlogService) { }

  ngOnInit(): void {
    this.blogs$ = this.blogService.getblog();



  }

}
