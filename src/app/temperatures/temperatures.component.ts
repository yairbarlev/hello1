import { Weather } from './../interfaces/weather';
import { Observable } from 'rxjs';
import { WheaterService } from './../wheater.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit{
city;
image;
temperature;
country;
description;
lat:number;
lon:number;
hasError = false;
errorMessage:string;
WheaterData$ :Observable<Weather>;
constructor (private route:ActivatedRoute, private weatherService:WheaterService) { }

ngOnInit(): void {
  this.city = this.route.snapshot.params.city;
  this.WheaterData$ = this.weatherService.searchWeatherData(this.city);
  this.WheaterData$ .subscribe(
    data => {
      this.temperature = data.temperature;
      this.image = data.image;
      this.country = data.country;
      this.description = data.description;
      this.lat = data.lat;
      this.lon = data.lon;
    },
    error =>{
      console.log(error.message);
      this.hasError = true;
      this.errorMessage = error.message;
    }
  )
}

}
