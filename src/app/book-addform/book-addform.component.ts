import { Book } from './../interfaces/book';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'bookaddform',
  templateUrl: './book-addform.component.html',
  styleUrls: ['./book-addform.component.css']
})
export class BookAddformComponent implements OnInit {


  @Input() title:string;   // decoration
  @Input() author:string;   // decoration
  @Output() close = new EventEmitter<null>();
  @Output() add = new EventEmitter<Book>();

  updateParent(){
    let book:Book = {title:this.title, author:this.author}
    this.add.emit(book);
  }
  closeForm(){
    this.close.emit();
  }
  constructor(public authService:AuthService) { }

  ngOnInit(): void {

  }

}
