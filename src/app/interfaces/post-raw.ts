export interface PostRaw {

  [index: number]: {
      userId:number,
    id:number,
    title: string,
    body: string
  }

}
