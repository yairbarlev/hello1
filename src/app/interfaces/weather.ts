export interface Weather {
  name:string,
  country:string,
  image:string,
  description:string,
  temperature:number,
  lat?:number,  //not mandatory
  lon?:number  //not mandatory
}
