import { Book } from './../interfaces/book';
import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})

export class BooksComponent implements OnInit {
  books; /**Same as "public books" */
  books$; /** $ - for observable*/
  userId:string;
  panelOpenState = false;
  editState = [];
  editStateAdd:boolean = false;
  constructor(private BooksService:BooksService, public authService:AuthService) { }/**BookService is of type BookService */
  deleteBook(id:string){
    this.BooksService.deleteBook(this.userId,id);

  }
  update(book:Book){
    this.BooksService.updateBook(this.userId, book.id,book.title,book.author);
  }
  addBook(book:Book){
    this.BooksService.addBook(this.userId, book.title,book.author);
  }
  ngOnInit(): void {
    /*this.books = this.BooksService.getBooks(); for syncrhonied data */
    //this.BooksService.addBooks();
    this.authService.getuser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.books$ = this.BooksService.getBooks(this.userId);
      }
    )
    //this.books$.subscribe(books => this.books = books); /**subscribing to an oservable can use async | in template instead*/
  }

}
