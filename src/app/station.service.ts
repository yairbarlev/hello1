import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StationService {
  stations= ['BBC', 'CNN', 'NBC'];

  public getFavorite( href:string){
    if(href === '/classifyes/BBC'){
      return 'BBC';
    }
    if(href === '/classifyes/NBC'){
      return 'NBC';
    }
    if(href === '/classifyes/CNN'){
      return 'CNN';
    }
    else{
      return 'No Such station';
    }
  }

  public getStations(){
    return this.stations;
    /**return this.books;*/
  }


  constructor() { }
}
