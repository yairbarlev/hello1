import { environment } from './../environments/environment.prod';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatRadioModule} from '@angular/material/radio';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { ClassifyesComponent } from './classifyes/classifyes.component';
import { HttpClientModule } from '@angular/common/http';
import { CityFormComponent } from './city-form/city-form.component';
import {MatSelectModule} from '@angular/material/select';
import { PostsComponent } from './posts/posts.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './register/register.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { BookFormComponent } from './book-form/book-form.component';
import { BookAddformComponent } from './book-addform/book-addform.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    TemperaturesComponent,
    ClassifyesComponent,
    CityFormComponent,
    PostsComponent,
    LoginComponent,
    RegisterComponent,
    BookFormComponent,
    BookAddformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatExpansionModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    FormsModule,
    MatSelectModule,
    HttpClientModule,
    MatListModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    MatInputModule,
    AngularFirestoreModule,



  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
