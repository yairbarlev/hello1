import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Post } from './interfaces/post';
import { PostRaw } from './interfaces/post-raw';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";


  constructor(private http:HttpClient) { }



  getblog(){
    return this.http.get<PostRaw>(`${this.URL}`);
    // const postsObservable = new Observable(observer => {
     // setInterval(()=>observer.next(this.blogs$),5)
   // }); /** callBack function  */s
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'server error');
  }


}
