import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css']
})
export class CityFormComponent implements OnInit {

  cities:object[] = [{id:1,name:'Jerusalem'},{id:2,name:'London'},{id:3,name:'paris'},{id:4,name:'manyak'}] //json
  city:string = 'jerusalem';
  onSubmit(){
    console.log(this.router);
    this.router.navigate(['/temperatures',this.city]);
  }
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

}
