import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreModule } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/internal/operators/map';

@Injectable({  /**Decoration*/
  providedIn: 'root'
})
export class BooksService {


  // books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:'What is Lorem IpsumLoand more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'},
  // {title:'The Magic Mountain', author:'Thomas Mann', summary:'What is Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop sum.'}];


  // public addBooks (){
  //   setInterval(()=>this.books.push({title:'A new one', author:'New author', summary:'What is Lorem IpsumLorem Ipsu'}),2000);

  // }

// public getBooks(){
//   const booksObservable = new Observable(observer => {
//     setInterval(()=>observer.next(this.books),500)
//   }); /** callBack function  */
//   return booksObservable;
//   /**return this.books;*/
// }


bookCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection('users');

public getBooks(userId){//משיכה סטנדרטית של מידע מfirestore
 this.bookCollection = this.db.collection(`users/${userId}/books`);
 return this.bookCollection.snapshotChanges().pipe(map(//observable map
   collection =>collection.map(//colection map
     document => {
       const data = document.payload.doc.data();
       data.id = document.payload.doc.id;
       return data;
     }
   )
 ))
}

addBook(userId:string ,title:string,author:string){
  const book = {title:title, author:author};
  this.userCollection.doc(userId).collection('books').add(book);
}
deleteBook(userId:string ,id:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}
updateBook(userId:string ,id:string,title:string,author:string){
  this.db.doc(`users/${userId}/books/${id}`).update(
    {
      title:title,
      author:author,
    }
  );
}


// getBooks(userId):Observable<any[]>{

// return
// }
  constructor(private db:AngularFirestore) { }
}
