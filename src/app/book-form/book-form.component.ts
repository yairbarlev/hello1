import { Book } from './../interfaces/book';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'bookform',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {


  @Input() title:string;   // decoration
  @Input() author:string;   // decoration
  @Input() id:string;       // decoration
  @Output() update = new EventEmitter<Book>();
  @Output() close = new EventEmitter<null>();
  updateParent(){
    let book:Book = {id:this.id,title:this.title, author:this.author}
    this.update.emit(book);
  }
  closeForm(){
    this.close.emit();
  }
  constructor() { }

  ngOnInit(): void {
  }

}
