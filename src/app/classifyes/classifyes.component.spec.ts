import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifyesComponent } from './classifyes.component';

describe('ClassifyesComponent', () => {
  let component: ClassifyesComponent;
  let fixture: ComponentFixture<ClassifyesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassifyesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifyesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
