import { StationService } from './../station.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'classifyes',
  templateUrl: './classifyes.component.html',
  styleUrls: ['./classifyes.component.css']
})
export class ClassifyesComponent implements OnInit {
  favoriteStation;
  stations;
  href;
  constructor(private stationsService:StationService, private router: Router) { }

  ngOnInit(): void {
    this.href = this.router.url;
    this.stations = this.stationsService.getStations();
    this.favoriteStation = this.stationsService.getFavorite(this.href);
  }

}

