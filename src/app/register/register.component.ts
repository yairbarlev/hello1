import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  email:string;
  password:string;
  onSubmit(){
    this.auth.register(this.email,this.password);
  }
  constructor(public auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
